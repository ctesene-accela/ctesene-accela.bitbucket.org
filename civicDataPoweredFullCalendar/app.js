var config = {
  baseUri: 'http://www.civicdata.com/api/3/action/datastore_search',
  resourceId: 'e03543d2-20fe-4f47-a829-e96c3c113f11',
  orderBy: 'DATE%20OPENED%20DESC',
  numberOfRecords: 200,
  uri: function() {
    return this.baseUri + '?resource_id=' + this.resourceId + '&sort=' + this.orderBy + '&limit=' + this.numberOfRecords;
  }
};

function setupCalendar(events) {
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    editable: false,
    events: events,
    eventClick: handleEventClick
  });
}

function createClassNameByRecordStatus(status) {
  return status.replace(/\s/g, '').toLowerCase();
}

function handleResponse(response) {
  var records = response.result.records;

  var events = _.map(records, function(item) {
    return {
      title: item['DESCRIPTION'],
      start: moment(item['DATE OPENED']),
      className: createClassNameByRecordStatus(item['RECORD STATUS']),
      address: item['ADDRESS'],
      business: item['BUSINESS NAME'],
      allDay: true,
      stick: true
    };
  });

  setupCalendar(events);
}

function handleError(xhr, status) {
  console.error(xhr);
}

function handleEventClick( event, jsEvent, view ) {
  alert(event.business + ' : ' + event.address);
}


$(document).ready(function () {
  $.ajax({
    url: config.uri(),
    type: "GET",
    crossDomain: true,
    dataType: "json",
    success: handleResponse,
    error: handleError
  });
});